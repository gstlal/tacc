# tacc

The home to start working on code to run workflows at TACC

## Running Workflow (Singularity)

#### 1. Build Singularity image

To pull a container with gstlal and the tacc library installed, run:

```
$ singularity build --sandbox --fix-perms <image-name> docker://containers.ligo.org/gstlal/tacc:master
```

#### 2. Set up workflow

To grab default data files (bank, PSD, workflow, config), run `tacc_init` where
you want to run the workflow, i.e.

```
$ cd /path/to/workflow/dir
$ singularity exec <image> tacc_init
```

Add this to `stage.site` and `inspiral.site` sections of `gstlal_inspiral_workflow.yml`:

`singularity-image: /path/to/image`

Next, set up your proxy to ensure you can get access to LIGO data:

```
$ ligo-proxy-init albert.einstein
$ make x509_proxy
```

Note that we are running this step outside of Singularity. This is because `ligo-proxy-init`
is not installed within the image currently.

If you haven't installed site-specific profiles yet, you can run:

```
$ singularity exec <image> tacc-profile install
```

which will install configurations that are site-specific, i.e. `ldas` and `ics`.
You can select which profile to use in the `*.site` sections of `gstlal_inspiral_workflow.yml`:

`profile: ldas`

To view which profiles are available, you can run:

```
$ singularity exec <image> tacc-profile list
```

Note, you can install custom profiles as well, see the "Installing custom site profiles" section.

Finally, set up the rest of the workflow:

```
$ singularity exec -B $TMPDIR <image> make
```

This should create condor DAGs for the staging and inspiral workflows. Mounting a temporary directory
is important as some of the steps will leverage a temporary space to generate files.

#### 3. Launch workflows

Stage workflow:

```
$ condor_submit_dag stage_dag.dag
```

Inspiral workflow:

```
$ condor_submit_dag trigger_dag.dag
```

## Installing Custom Site Profiles

You can define a site profile as YAML. As an example, we can create a file called `custom.yml`:

```
scheduler: condor
requirements:
  - "(IS_GLIDE_IN=?=True)"
directives:
  +OpenScienceGrid: true
```

Both the directives and requirements section are optional.

To install one so it's available within the TACC workflow, run:

```
$ singularity exec <image> tacc-profile install custom.yml
```
