FROM containers.ligo.org/lscsoft/gstlal:master

USER root

# install condor
RUN cd /etc/yum.repos.d && \
    wget http://research.cs.wisc.edu/htcondor/yum/repo.d/htcondor-development-rhel7.repo
RUN yum install -y python3-condor

# install dependencies
RUN python3 -m pip install dataclasses

# install tacc library
COPY dist /dist
RUN cd /dist && \
    tar -xzf tacc*.tar.gz && \
    cd tacc* && \
    python3 setup.py install --root=/ --single-version-externally-managed
RUN rm -rf /dist

ENTRYPOINT []
CMD bash
