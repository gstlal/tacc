#!/usr/bin/env python3

import argparse
import getpass
import os
import sys

from htcondor import dags
import yaml

from tacc import dagparts, profiles
from tacc.dagparts import Layer, Node, Argument


parser = argparse.ArgumentParser()
parser.add_argument("--num-cores", required=True, help="number of cores to request")
parser.add_argument("-c", "--config", required=True,
	help="sets the workflow configuration file to use")
parser.add_argument('tarballs', nargs='+')

# parse arguments + config
args = parser.parse_args()
with open(args.config, "r") as f:
	config = yaml.safe_load(f)["stage"]["site"]

# DAG set up
dagparts.create_log_dir()
outdir = os.path.join(os.getcwd(), "trigger_data")
dag = dags.DAG()

# common condor options
if "accounting-group-user" in config:
	accounting_group_user = config["accounting-group-user"]
else:
	accounting_group_user = getpass.getuser()

condor_opts = {
	"request_cpus": args.num_cores,
	"request_memory": int(args.num_cores) * 4000,
	"want_graceful_removal": "True",
	"kill_sig": "15",
	"accounting_group": config["accounting-group"],
	"accounting_group_user": accounting_group_user,
}
requirements = []

# load site profile
profile = profiles.load_profile(config["profile"])
assert profile["scheduler"] == "condor", "only the condor scheduler is available"

# add profile-specific options
if "directives" in profile:
	condor_opts.update(profile["directives"])
if "requirements" in profile:
	requirements.extend(profile["requirements"])

# singularity-specific options
if "singularity-image" in config:
	singularity_image = config["singularity-image"]
	requirements.append("(HAS_SINGULARITY=?=True)")
	condor_opts['+SingularityImage'] = f'"{singularity_image}"'
	condor_opts['x509userproxy'] = 'x509_proxy'
	condor_opts['use_x509userproxy'] = True
	condor_opts['transfer_executable'] = False
	condor_opts['getenv'] = False

# condor requirements
condor_opts['requirements'] = " && ".join(requirements)

# dag generation
stage_layer = Layer("stage", requirements=condor_opts)

for tar in args.tarballs:
	ftar = tar.replace(".tar", "-frames.tar")
	tarfiles = [Argument("tarball", tar), Argument("frame_tarball", ftar)]
	stage_layer += Node(
		arguments=[Argument("num_cores", args.num_cores)] + tarfiles,
		input_files=tarfiles[:1],
		output_files=tarfiles,

	)
dag.layer(**stage_layer.config(), retries=3)

dagparts.write_dag(dag, dag_file_name="stage_dag.dag")
