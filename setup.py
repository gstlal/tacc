import glob
from setuptools import find_packages, setup

# requirements
install_requires = [
    "dataclasses ; python_version < '3.7'",
]

setup(
    name = 'tacc',
    description = 'TACC workflow for GstLAL',
    version = '0.0.1',
    author = 'Patrick Godwin',
    author_email = 'patrick.godwin@ligo.org',
    url = 'https://git.ligo.org/gstlal/tacc.git',
    license = 'GPLv2+',

    packages = find_packages(),
    package_data = {
        'tacc.config': ['*'],
        'tacc.data': ['*'],
        'tacc.profiles': ['*'],
        'tacc.templates': ['*'],
        'tacc.workflow': ['*'],
    },
    data_files = [
        ('bin', glob.glob('bin/*')),
    ],
    entry_points = {
        'console_scripts': [
            'tacc-profile = tacc.profiles:main',
        ],
    },

    install_requires = install_requires,
    zip_safe=False,
)
