from collections.abc import Iterable
from dataclasses import dataclass, field
import os
from typing import List, Tuple, Union

import htcondor
from htcondor import dags

from gstlal.dagparts import which


@dataclass
class Layer:
    executable: str
    name: str = ""
    universe: str = "vanilla"
    log_dir: str = "logs"
    requirements: dict = field(default_factory=dict)
    nodes: list = field(default_factory=list)

    def __post_init__(self):
        if not self.name:
            self.name = os.path.basename(self.executable)

    def config(self):
        # check that nodes are valid
        self.validate()

        # add base submit opts + requirements
        submit_options = {
            "universe": self.universe,
            "executable": which(self.executable),
            "arguments": self._arguments(),
            **self.requirements,
        }

        # file submit opts
        input_files = self._input_files()
        output_files = self._output_files()
        if input_files or output_files:
            submit_options["should_transfer_files"] = "YES"
            submit_options["when_to_transfer_output"] = "ON_SUCCESS"
            submit_options["success_exit_code"] = 0
            submit_options["preserve_relative_paths"] = True
        if input_files:
            submit_options["transfer_input_files"] = input_files
        if output_files:
            submit_options["transfer_output_files"] = output_files

        # log submit opts
        submit_options["output"] = f"{self.log_dir}/$(nodename)-$(cluster)-$(process).out"
        submit_options["error"] = f"{self.log_dir}/$(nodename)-$(cluster)-$(process).err"

        # extra boilerplate submit opts
        submit_options["notification"] = "never"

        return {
            "name": self.name,
            "submit_description": htcondor.Submit(submit_options),
            "vars": self._vars(),
        }

    def append(self, node):
        self.nodes.append(node)

    def extend(self, nodes):
        self.nodes.extend(nodes)

    def __iadd__(self, nodes):
        if isinstance(nodes, Iterable):
            self.extend(nodes)
        else:
            self.append(nodes)
        return self

    def validate(self):
        assert self.nodes, "at least one node must be connected to this layer"

        # check arg names across nodes are equal
        args = [arg.name for arg in self.nodes[0].arguments]
        for node in self.nodes[:-1]:
            assert args == [arg.name for arg in node.arguments]

        # check input/output names across nodes are equal
        inputs = [arg.name for arg in self.nodes[0].input_files]
        for node in self.nodes[:-1]:
            assert inputs == [arg.name for arg in node.input_files]
        outputs = [arg.name for arg in self.nodes[0].output_files]
        for node in self.nodes[:-1]:
            assert outputs == [arg.name for arg in node.output_files]

    def _arguments(self):
        return " ".join([f"$({arg.name})" for arg in self.nodes[0].arguments])

    def _input_files(self):
        return ",".join([f"$(input_{arg.name})" for arg in self.nodes[0].input_files])

    def _output_files(self):
        return ",".join([f"$(output_{arg.name})" for arg in self.nodes[0].output_files])

    def _vars(self):
        allvars = []
        for i, node in enumerate(self.nodes):
            nodevars = {arg.name: arg.vars() for arg in node.arguments}
            nodevars["nodename"] = f"{self.name}_{i:04X}"
            if node.input_files:
                nodevars.update({f"input_{arg.name}": arg.files() for arg in node.input_files})
            if node.output_files:
                nodevars.update({f"output_{arg.name}": arg.files() for arg in node.output_files})
            allvars.append(nodevars)

        return allvars


@dataclass
class Node:
    arguments: list = field(default_factory=list)
    input_files: list = field(default_factory=list)
    output_files: list = field(default_factory=list)


@dataclass
class Argument:
    name: str
    argument: Union[str, List]

    def vars(self):
        if isinstance(self.argument, str):
            return f"{self.argument}"
        else:
            return " ".join(self.argument)

    def files(self):
        if isinstance(self.argument, str):
            return f"{self.argument}"
        else:
            return ",".join(self.argument)


@dataclass
class Option:
    name: str
    argument: Union[None, str, List] = None

    def vars(self):
        if not self.argument:
            return f"--{self.name}"
        elif isinstance(self.argument, str):
            return f"--{self.name} {self.argument}"
        else:
            return " ".join([f"--{self.name} {arg}" for arg in self.argument])


class HexFormatter(dags.SimpleFormatter):
    """A hex-based node formatter that produces names like LayerName_000C.

    """
    def __init__(self, offset: int = 0):
        self.separator = "_"
        self.index_format = "{:04X}"
        self.offset = offset

    def parse(self, node_name: str) -> Tuple[str, int]:
        layer, index = node_name.split(self.separator)
        index = int(index, 16)
        return layer, index - self.offset


def create_log_dir(log_dir="logs"):
    os.makedirs(log_dir, exist_ok=True)


def write_dag(dag, dag_dir=None, node_name_formatter=None, **kwargs):
    if not node_name_formatter:
        node_name_formatter = HexFormatter()
    if not dag_dir:
        dag_dir = os.getcwd()
    return htcondor.dags.write_dag(dag, dag_dir, node_name_formatter=node_name_formatter, **kwargs)
