import argparse
import fnmatch
import os
import shutil
import pkg_resources

import jinja2
import yaml


def get_profile_path():
    """Get the default location for site profiles.

    """
    return os.path.join(os.getenv("HOME"), ".config", "tacc")


def installed_profiles():
    return [f[:-4] for f in os.listdir(get_profile_path()) if fnmatch.fnmatch(f, '*.yml')]


def current_profile():
    current_profile = os.path.join(get_profile_path(), "current.txt")
    if not os.path.exists(current_profile):
        raise ValueError("no current profile selected")
    with open(current_profile, "r") as f:
        return f.read()


def load_profile(profile=None):
    if not profile:
        profile = current_profile()
    profile_loc = os.path.join(get_profile_path(), f'{profile}.yml')
    with open(profile_loc, "r") as f:
        return yaml.safe_load(f)


def install_profiles(args):
    """Install site profiles in a central config location.

    """
    os.makedirs(get_profile_path(), exist_ok=True)
    if args.profile:
        profiles = [f for f in args.profile if fnmatch.fnmatch(f, '*.yml')]
    else:
        files = pkg_resources.resource_listdir('tacc', 'profiles')
        profiles = [f for f in files if fnmatch.fnmatch(f, '*.yml')]
        profiles = [pkg_resources.resource_filename('tacc', os.path.join('profiles', f)) for f in profiles]
    for profile in profiles:
        shutil.copy2(profile, get_profile_path())


def list_profiles(args):
    """List currently installed site profiles.

    """
    print("installed site profiles:")
    print("\t" + " ".join(installed_profiles()))


def get_profile(args):
    """Display currently selected site profile.

    """
    try:
        current = current_profile()
    except ValueError:
        print("no current profile selected")
    else:
        print(f"current profile: {current}")


def set_profile(args):
    if args.profile not in installed_profiles():
        print("invalid profile selection, run tacc-profile list to get a list of valid profiles")
    with open(os.path.join(get_profile_path(), "current.txt"), "w") as f:
        f.write(args.profile)


def main():
    parser = argparse.ArgumentParser(prog="tacc-profile")

    # set up subparser
    subparser = parser.add_subparsers(title="commands", metavar="<command>", dest="cmd")
    subparser.required = True

    # register commands
    install_desc = "install site submission profiles"
    install_parser = subparser.add_parser("install", help=install_desc, description=install_desc)
    install_parser.set_defaults(func=install_profiles)
    install_parser.add_argument(
        "profile", metavar="PROFILE", nargs="*",
        help="Profiles to install. If none selected, install default profiles."
    )

    list_desc = "list available site submission profiles"
    list_parser = subparser.add_parser("list", help=list_desc, description=list_desc)
    list_parser.set_defaults(func=list_profiles)

    set_desc = "set site submission profile"
    set_parser = subparser.add_parser("set", help=set_desc, description=set_desc)
    set_parser.set_defaults(func=set_profile)
    set_parser.add_argument("profile", metavar="PROFILE", help="profile to select.")

    get_desc = "display current site submission profile"
    get_parser = subparser.add_parser("get", help=get_desc, description=get_desc)
    get_parser.set_defaults(func=get_profile)

    # parse and execute command
    args = parser.parse_args()
    args.func(args)
